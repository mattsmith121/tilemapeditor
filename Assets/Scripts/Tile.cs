﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tilemap Tools/Tile")]
public class Tile : ScriptableObject
{
    [SerializeField]
    public Sprite tileSprite;
}
