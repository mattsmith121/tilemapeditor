﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tilemap Tools/Tilemap")]
public class CustomTilemap : ScriptableObject
{
    [SerializeField]
    public List<Tile> map = new List<Tile>(100);
}
