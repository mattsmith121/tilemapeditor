using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System;
using System.Collections.Generic;

public class TilemapEditor : EditorWindow
{
    struct Coords
    {
        public int x;
        public int y;

        public Coords(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    enum EditMode
    {
        Paint,
        Delete
    }

    CustomTilemap tilemap;
    VisualElement right_panel;
    VisualElement left_panel;
    Tile selectedTile;
    EditMode editMode;
    Button paintButton, deleteButton;

    [MenuItem("Window/Project/TilemapEditor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("TilemapEditor");
    }

    public void OnEnable() {
        // tilemap should start null
        tilemap = null;

        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement main_panel = new VisualElement();
        main_panel.style.flexGrow = 1.0f;
        main_panel.style.flexDirection = FlexDirection.Row;
        main_panel.style.borderTopWidth = main_panel.style.borderRightWidth = main_panel.style.borderBottomWidth = main_panel.style.borderLeftWidth = 5.0f;
        main_panel.style.borderTopColor = main_panel.style.borderRightColor = main_panel.style.borderBottomColor = main_panel.style.borderLeftColor = Color.black;
        root.Add(main_panel);

        #region Left Panel Root
        left_panel = new VisualElement();
        left_panel.style.flexDirection = FlexDirection.Column;
        left_panel.style.flexGrow = 0.25f;
        //left_panel.style.minWidth = 150f;
        left_panel.style.borderTopWidth = left_panel.style.borderRightWidth = left_panel.style.borderBottomWidth = left_panel.style.borderLeftWidth = 5.0f;
        left_panel.style.borderTopColor = left_panel.style.borderRightColor = left_panel.style.borderBottomColor = left_panel.style.borderLeftColor = Color.blue;
        main_panel.Add(left_panel);

        ObjectField tilemapObjectField = new ObjectField("Tilemap");
        tilemapObjectField.objectType = typeof(CustomTilemap);
        tilemapObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnTilemapChanged);
        left_panel.Add(tilemapObjectField);

        paintButton = new Button() {
            text = "Paint",
            style = {
                width = 100,
                height = 50
            }
        };
        paintButton.clicked += () => SwitchMode(true);
        left_panel.Add(paintButton);

        deleteButton = new Button() {
            text = "Delete",
            style = {
                width = 100,
                height = 50
            }
        };
        deleteButton.clicked += () => SwitchMode(false);
        left_panel.Add(deleteButton);

        // Create list of tiles
        SetTileList();

        #endregion

        #region Right Panel Root
        right_panel = new VisualElement();
        right_panel.style.flexDirection = FlexDirection.Column;
        right_panel.style.flexGrow = 1.0f;
        right_panel.style.borderTopWidth = right_panel.style.borderRightWidth = right_panel.style.borderBottomWidth = right_panel.style.borderLeftWidth = 5.0f;
        right_panel.style.borderTopColor = right_panel.style.borderRightColor = right_panel.style.borderBottomColor = right_panel.style.borderLeftColor = Color.red;
        main_panel.Add(right_panel);

        // Create view of tilemap
        SetTiles();
        #endregion

        // default to paint mode
        SwitchMode(true);
    }

    private void SetTileList() {
        // Create some list of data
        Tile[] tiles = Resources.LoadAll<Tile>("Tilemap Objects");

        // The "makeItem" function will be called as needed
        // when the ListView needs more items to render
        Func<VisualElement> makeItem = () => {
            Image image = new Image();
            image.style.width = 50f;
            image.style.height = 50f;
            image.style.paddingTop = image.style.paddingBottom = image.style.paddingRight = image.style.paddingLeft = 5f;
            image.styleSheets.Add(Resources.Load<StyleSheet>("SelectedStyle"));
            return image;
        };

        // As the user scrolls through the list, the ListView object
        // will recycle elements created by the "makeItem"
        // and invoke the "bindItem" callback to associate
        // the element with the matching data item (specified as an index in the list)
        Action<VisualElement, int> bindItem = (e, i) => (e as Image).sprite = tiles[i].tileSprite;

        // Provide the list view with an explict height for every row
        // so it can calculate how many items to actually display
        const int itemHeight = 50;

        var listView = new ListView(tiles, itemHeight, makeItem, bindItem);

        listView.selectionType = SelectionType.Multiple;

        listView.onSelectionChange += objects => {
            // Set selected tile to selected item
            selectedTile = listView.selectedItem as Tile;
        };
        var ht = listView.Children();
        listView.selectionType = SelectionType.Single;
        listView.style.flexGrow = 1.0f;
        left_panel.Add(listView);
    }

    private void OnMouseUpEvent_Image(MouseUpEvent _event, Coords pos) {
        if (tilemap == null)
            return;
        int x = pos.x;
        int y = pos.y;
        if (tilemap != null) {
            if (editMode == EditMode.Delete) {
                tilemap.map[y*10 + x] = null;
            }
            else {
                tilemap.map[y*10 + x] = selectedTile;
            }
        }

        Image image = _event.target as Image;
        if (image != null) {
            SetImageSprite(image, x, y);
        }
    }

    private void SetTiles() {
        // Clear panel first
        right_panel.Clear();

        // Fill Tiles
        for (int j = 0; j < 10; j++) {
            VisualElement image_row_panel = new VisualElement();
            image_row_panel.style.flexDirection = FlexDirection.Row;
            right_panel.Add(image_row_panel);

            for (int i = 0; i < 10; i++) {
                Box box = new Box();
                box.style.minWidth = 64;
                box.style.minHeight = 64;
                box.style.maxWidth = 64;
                box.style.maxHeight = 64;
                box.style.paddingBottom = box.style.paddingTop = box.style.paddingRight = box.style.paddingLeft = 1f;
                box.style.borderBottomColor = box.style.borderTopColor = box.style.borderRightColor = box.style.borderLeftColor = Color.white;
                box.RegisterCallback<MouseUpEvent, Coords>(OnMouseUpEvent_Image, new Coords(i, j));
                image_row_panel.Add(box);

                Image image = new Image();
                image.style.flexGrow = 1.0f;
                SetImageSprite(image, i, j);
                box.Add(image);
            }
        }
    }

    private void OnTilemapChanged(ChangeEvent<UnityEngine.Object> evt) {
        tilemap = evt.newValue as CustomTilemap;
        if (tilemap != null) {
            EditorUtility.SetDirty(tilemap);
        }
        SetTiles();
    }

    private void SetImageSprite(Image image, int x, int y) {
        if (tilemap == null || tilemap.map[y*10 + x] == null || tilemap.map[y*10 + x].tileSprite == null) {
            image.sprite = null;
        }
        else {
            image.sprite = tilemap.map[y*10 + x].tileSprite;
        }
    }

    private void SwitchMode(bool paint) {
        paintButton.style.color = paint ? Color.yellow : Color.grey;
        deleteButton.style.color = !paint ? Color.yellow : Color.grey;
        editMode = paint ? EditMode.Paint : EditMode.Delete;
    }
}